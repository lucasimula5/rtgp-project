/*

00_basic.vert : basic Vertex shader

author: Davide Gadia

Real-Time Graphics Programming - a.a. 2021/2022
Master degree in Computer Science
Universita' degli Studi di Milano

*/


#version 410 core

precision mediump float;

// vertex position in world coordinates
// the number used for the location in the layout qualifier is the position of the vertex attribute
// as defined in the Mesh class
layout (location = 0) in vec3 position;
// vertex normal in world coordinate
layout (location = 1) in vec3 normal;
// UV texture coordinates
layout (location = 2) in vec2 UV;

// model matrix
uniform mat4 modelMatrix;
// view matrix
uniform mat4 viewMatrix;
// Projection matrix
uniform mat4 projectionMatrix;

out vec3 fragPosition;
// the transformed normal (in view coordinate) is set as an output variable, to be "passed" to the fragment shader
// this means that the normal values in each vertex will be interpolated on each fragment created during rasterization between two vertices
out vec3 vNormal;

// the output variable for UV coordinates
out vec2 interp_UV;

void main()
{
    interp_UV = UV;
    /*
    fragPosition = vec3(modelMatrix * vec4(position, 1.0));

    // transformations are applied to the normal
    vNormal = normalize( normalMatrix * normal );

    // transformations are applied to each vertex
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0f);
    */
    fragPosition = vec3(modelMatrix * vec4(position, 1.0));
    vNormal = mat3(transpose(inverse(modelMatrix))) * normal;
    gl_Position = projectionMatrix * viewMatrix * vec4(fragPosition, 1.0);

}
