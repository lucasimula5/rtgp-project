/*

01_fullcolor.frag : basic Fragment shader, it applies an uniform color to all the fragments. Color is passed as uniform from the main application

N.B.)  "00_basic.vert" must be used as vertex shader

author: Davide Gadia

Real-Time Graphics Programming - a.a. 2021/2022
Master degree in Computer Science
Universita' degli Studi di Milano

*/

#version 410 core

precision mediump float;

// output shader variable
out vec4 colorFrag;

// color to assign to the fragments: it is passed from the application
uniform vec3 colorIn;

// UV texture coordinates, interpolated in each fragment by the rasterization process
in vec2 interp_UV;

uniform vec2 seeds[32];
uniform bool hit;

in vec3 fragPosition;
// the transformed normal (in view coordinate) is set as an output variable, to be "passed" to the fragment shader
// this means that the normal values in each vertex will be interpolated on each fragment created during rasterization between two vertices
in vec3 vNormal;

vec3 glassColor = vec3(0.15f, 0.59f, 0.75f);
vec3 black = vec3(1.0f);

uniform vec3 viewPos;

float custom_dist(vec2 seed, vec2 uv) {
    vec2 newSeed = seed;
    if (distance(seed.x, uv.x) > distance(seed.x - 1, uv.x)) {
        newSeed.x -= 1.0f;
    }

    if (distance(seed.x, uv.x) > distance(seed.x + 1, uv.x)) {
        newSeed.x += 1.0f;
    }

    if (distance(seed.y, uv.y) > distance(seed.y - 1, uv.y)) {
        newSeed.y -= 1.0f;
    }

    if (distance(seed.y, uv.y) > distance(seed.y + 1, uv.y)) {
        newSeed.y += 1.0f;
    }

    return distance(newSeed, uv);
}

void main() {
    // Calculate Fresnel effect
    float R0 = 0.04;
    float cosTheta = dot(normalize(viewPos - fragPosition), normalize(vNormal));
    float fresnel = R0 + (1.0 - R0) * pow(1.0 - cosTheta, 5.0);

    // Calculate reflection and refraction
    vec3 reflectDir = reflect(-normalize(viewPos - fragPosition), normalize(vNormal));
    vec3 refractDir = refract(-normalize(viewPos - fragPosition), normalize(vNormal), 1.0 / 1.52); // Assuming glass refractive index of 1.52

    //vec3 reflectColor = texture(reflectionTexture, reflectDir).rgb;
    //vec3 refractColor = texture(refractionTexture, refractDir).rgb;

    // Final color with Fresnel effect
    vec3 finalColor = mix(reflectDir, refractDir, fresnel);

    colorFrag = vec4(finalColor, 1.0);
}
